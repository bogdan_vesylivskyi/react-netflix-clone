import React, {useContext} from "react"
import {BrowserRouter as Router, Switch} from "react-router-dom"
import * as ROUTES from "./constants/routes"
import {Home, Signin, Signup, Browse, Profiles} from "./pages"
import {useAuthListener} from "./hooks"
import {IsUserRedirect, ProtectedRoute} from "./helpers/routes"
import {FirebaseContext} from "../src/context/firebase";
import {FooterContainer} from "./containers/footer";
import Starred from './pages/Starred';
import {ThemeProvider} from 'styled-components';

// global theme
const theme = {
    mainColors: {
        red: '#d81f26',
        gray: '#c6c6c6',
        dark: '#353535',
    },
};

export function App() {
    const {user} = useAuthListener()
    const {firebase} = useContext(FirebaseContext)

    return (
        <Router>
            <div className="wrap">
                {/*<Header src="header-bg" dontShowOnSmallViewPort>*/}
                {/*    <HeaderContainer user={user} firebase={firebase}/>*/}
                {/*</Header>*/}
                <ThemeProvider theme={theme}>

                <Switch>
                    <IsUserRedirect user={user} loggedInPath={ROUTES.BROWSE} path={ROUTES.SIGN_IN}><Signin/></IsUserRedirect>
                    <IsUserRedirect user={user} loggedInPath={ROUTES.BROWSE} path={ROUTES.SIGN_UP}><Signup/></IsUserRedirect>
                    <ProtectedRoute user={user} loggedInPath={ROUTES.BROWSE} path={ROUTES.PROFILES}><Profiles/></ProtectedRoute>
                    <ProtectedRoute user={user} path={ROUTES.BROWSE}><Browse/></ProtectedRoute>

                    <ProtectedRoute user={user} path={ROUTES.SEARCH}><Home/></ProtectedRoute>

                    <ProtectedRoute user={user} path={ROUTES.FAVORITE}><Starred/></ProtectedRoute>
                    <ProtectedRoute user={user} loggedInPath={ROUTES.BROWSE} path={ROUTES.HOME}><Home/></ProtectedRoute>
                </Switch>

                </ThemeProvider>
                <FooterContainer/>
            </div>
        </Router>
        )
}
