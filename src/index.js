import React from "react"
import ReactDOM from "react-dom"
import { GlobalStyle } from "./global-styles"
import { App } from "./App"
import { FirebaseContext } from "./context/firebase"
// import { seedDatabase } from '../src/seed';

const firebaseConfig = {
    apiKey: "AIzaSyDXBo5E-WxvSn2hi558Ld94k1APfCuPhwc",
    authDomain: "netflix-clone-2021-b61a7.firebaseapp.com",
    databaseURL: "https://netflix-clone-2021-b61a7-default-rtdb.firebaseio.com",
    projectId: "netflix-clone-2021-b61a7",
    storageBucket: "netflix-clone-2021-b61a7.appspot.com",
    messagingSenderId: "541077109547",
    appId: "1:541077109547:web:035f75a24eab711614636e"
}


// Initialize Firebase
// const firebase = window.firebase.initializeApp(firebaseConfig)
window.firebase.initializeApp(firebaseConfig)
// seedDatabase(firebase)

ReactDOM.render(
    <FirebaseContext.Provider value={{ firebase: window.firebase }}>
        <GlobalStyle />
        <App />
    </FirebaseContext.Provider>,
    document.getElementById("root")
)
