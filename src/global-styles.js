import {createGlobalStyle} from "styled-components"

export const GlobalStyle = createGlobalStyle`
    html, body {
        font-family: 'HelveticaNeue', sans-serif;
        color: #fff;
        font-size: 16px;
        margin: 0;
        background-color: #000;
        -webkit-font-smoothing: antialiased;
    }
    
    .app-wrapper {
        // display: grid;
        // grid-template-areas:
        // "header header"
        // "nav content";
        // grid-template-columns: 3fr 9fr;
        // padding: 40px;
        // background: #1f1d1d;  
        background: #fff;  
    }
    .app-wrapper .app-wrapper-content {
        // grid-area: content;
            padding: 40px 25px;
            background: #5f5b5b; 
    }
    .sidebar {
        padding: 40px;
        // background: #2f2e2e;
        background: #fff;
    }
    .tabLinks {
        background: rgb(95, 91, 91);
        padding: 30px 0;
    }
    .tabLinks ul {
        margin-bottom: 30px
    }
    .text-center{
    text-align: center;
    }
`
