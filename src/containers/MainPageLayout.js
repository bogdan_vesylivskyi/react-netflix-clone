import React, {useContext, useState} from 'react';
import Nav from './sidebar';
import Title from './Title';
import {Header} from "../components";
import * as ROUTES from "../constants/routes";
import {Link} from "react-router-dom";
import useAuthListener from "../hooks/use-auth-listener";
import {FirebaseContext} from "../context/firebase";

const MainPageLayout = ({ children }) => {
  const {user} = useAuthListener();
  const {firebase} = useContext(FirebaseContext)
  // const [profile, setProfile] = useState({displayName: user.displayName, email: user.email})

  return (<>
        <Header src="header-bg" dontShowOnSmallViewPort>
          <Header.Frame>
            <Header.Group>
              <Header.Logo
                  to={ROUTES.HOME}
                  src="/images/icons/logo.svg"
                  alt="Netflix"
              />
            </Header.Group>

            <Header.Group>
              <Link to={ROUTES.BROWSE}>Browse</Link>
            </Header.Group>

            <Header.Group>
              <Link to={ROUTES.PROFILES}>Profiles</Link>
            </Header.Group>


            <Nav/>

            <Header.Group>
              <Header.Profile>
                <Header.Picture src={user.photoURL}/>
                <Header.Dropdown>
                  <Header.Group>
                    <Header.Picture src={user.photoURL}/>
                    <Header.Link>
                      {user.displayName}
                    </Header.Link>
                  </Header.Group>
                  <Header.Group>
                    <Header.Link
                        onClick={() =>
                            firebase.auth().signOut()
                        } to="/browse"
                    >
                      Sign out
                    </Header.Link>
                  </Header.Group>
                </Header.Dropdown>

              </Header.Profile>
            </Header.Group>
          </Header.Frame>

          <Header.Feature>
            <Header.FeatureCallOut>
              Unlimited movies, TV shows, and more.
            </Header.FeatureCallOut>
            <Header.Text>
              Netflix is the home of amazing original programming that you can’t find anywhere else. Movies,
              TV shows, specials and more, all tailored specifically to you.
            </Header.Text>
          </Header.Feature>
        </Header>
        <div className="tabLinks">
          <Title
              title="Search & Favorites block"
          />
          <Nav />
          {children}
        </div>
  </>

  );
};

export default MainPageLayout;
