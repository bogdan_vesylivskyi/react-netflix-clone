import React from "react"
import { Footer } from "../components"

export function FooterContainer() {
    return (
        <Footer>

            <Footer.Break />
            <Footer.Row>
                <Footer.Column>
                    <Footer.Title>FAQs</Footer.Title>
                    <Footer.Link href="#">Link1</Footer.Link>
                    <Footer.Link href="#">Link2</Footer.Link>
                    <Footer.Link href="#">Link3</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Title>Get Help</Footer.Title>
                    <Footer.Link href="#">Link1</Footer.Link>
                    <Footer.Link href="#">Link2</Footer.Link>
                    <Footer.Link href="#">Link3</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Title>Our Company</Footer.Title>
                    <Footer.Link href="#">Link1</Footer.Link>
                    <Footer.Link href="#">Link2</Footer.Link>
                    <Footer.Link href="#">Link3</Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Title>Getting Started</Footer.Title>
                    <Footer.Link href="#">Link1</Footer.Link>
                    <Footer.Link href="#">Link2</Footer.Link>
                    <Footer.Link href="#">Link3</Footer.Link>
                </Footer.Column>
            </Footer.Row>
            <Footer.Break />
            <Footer.Text>© 2021 Netflix clone, Inc.</Footer.Text>
        </Footer>
    )
}
