import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Movies from "../movie/movies";
import Movie from "../movie/movie";
import React from "react";
import styled from "styled-components";

export default function Films({movies}) {

    const Container = styled.div`
      max-width: 1200px;
      margin: 0 auto;
      padding: 0 20px;
    `;

    return (<>
         <div className="app-wrapper">

            <div className="app-wrapper-content">
                <div className="text-center"><h1>All films</h1></div>
                <Router>
                    <Container>
                        <Switch>
                            <Route exact path="/browse" render={() => <Movies movies={movies}/>}/>
                            <Route path="/shows/:id" render={() => (<Movie/>)}/>
                        </Switch>
                    </Container>
                </Router>
            </div>
        </div>
        </>
    )
};