import {Header} from "../index";
import * as ROUTES from "../../constants/routes";
import React from "react";

export default function HeaderContainer({user, firebase}) {

    return (<>
        <Header.Frame>
            <Header.Group>
                <Header.Logo
                    to={ROUTES.HOME}
                    src="/images/icons/logo.svg"
                    alt="Netflix clone"
                />
            </Header.Group>
            <Header.Group>
                <Header.Profile>
                    <Header.Picture src={user.photoURL}/>
                  0  <Header.Dropdown>
                        <Header.Group>
                            <Header.Picture src={user.photoURL}/>
                            <Header.Link>
                                {user.displayName}
                            </Header.Link>
                        </Header.Group>

                        <Header.Group>
                            <Header.Link
                                onClick={() =>
                                    firebase.auth().signOut()
                                } to="/browse"
                            >
                                Sign out
                            </Header.Link>
                        </Header.Group>
                    </Header.Dropdown>

                </Header.Profile>
            </Header.Group>
        </Header.Frame>

        <Header.Feature>
            <Header.FeatureCallOut>
                Unlimited movies, TV shows, and more.
            </Header.FeatureCallOut>
            <Header.Text>
                Netflix is the home of amazing original programming that you can’t find anywhere else. Movies,
                TV shows, specials and more, all tailored specifically to you.
            </Header.Text>
        </Header.Feature>
    </>)
}