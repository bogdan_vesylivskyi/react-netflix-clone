import styled from 'styled-components';
import { SearchCard } from '../../../containers/styles/styled';

// extended from SearchCard
export const StyledActorCard = styled(SearchCard)`
  .deathday {
    margin: 0;
    margin-top: 15px;
    font-weight: bold;
  }
`;
