import React, {useEffect, useState} from "react";
import axios from "axios";
import {BASE_URL} from "../../api";
import {useLocation} from 'react-router-dom'

import './movieStyles.css'

const Movie = ({}) => {
    const [movie, setMovie] = useState(null);
    const location = useLocation();
    const pathname = location.pathname.split("/");
    const movieId = pathname[pathname.length - 1];

    useEffect(() => {
        axios.get(`${BASE_URL}/shows/${movieId}`).then(res => {
            setMovie(res.data)
        })
    }, []);

    // console.log(movie, 'movie')

    return (
        <>
            {!movie ?
                <div>Loading</div> :
                <div className="movieWrapper">
                    <div className="movieImage">
                        <img src={movie.image.medium} alt={movie.name}/>
                    </div>
                    <div className="movieDescription">
                        <h1>{movie.name}</h1>
                        <div>{movie.summary}</div>
                        <ul>
                            {movie.genres.map((genre, ind) => (
                                <li key={ind}>{genre}</li>
                            ))}
                        </ul>
                        <p>Premiered: {movie.premiered}</p>
                        <p>Rating: {movie.rating.average}</p>
                        <p>{movie.language}</p>
                        <div><br/><a href={movie.url} target='_blank'>link to watch {movie.name}</a></div>
                    </div>
                </div>
            }
        </>
    );
};

export default Movie;
