import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import './movieCard.css';


const MovieCard = ({movie: {image, name, rating, id, premiered, medium}}) => {
// console.log(name, id, 'movie')

    return (
        <div className='card'>
            <div className="imageWrapper">
                <img src={image.medium}/>
            </div>
            <Link
                className="link"
                to={`shows/${id}`}
            />
            <div className="cardContent">
                <h3 className="cardTitle">
                    {name} ({premiered.split("-")[0]})
                </h3>
                <span className="rating">{rating.average}</span>
            </div>

        </div>
    );
};

export default MovieCard;