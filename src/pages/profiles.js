import React, {useContext, useState} from "react";
import useAuthListener from "../hooks/use-auth-listener";
import * as ROUTES from "../constants/routes";
import {FirebaseContext} from "../context/firebase";

// import firebase from "firebase";
import "firebase/database";
import {Header} from "../components";
import {Link} from "react-router-dom";
import Nav from "../containers/sidebar";


export default function Profiles() {
    const {user} = useAuthListener();
    const {firebase} = useContext(FirebaseContext)
    // const [profile, setProfile] = useState({displayName: user.displayName, email: user.email})


    return (
        <div className="app-wrapper">

            {/*<SidebarContainer/>*/}

            {/*<div className="sidebar">*/}
            {/*    Sidebar*/}
            {/*</div>*/}

            <Header src="header-bg" dontShowOnSmallViewPort>
                <Header.Frame>
                    <Header.Group>
                        <Header.Logo
                            to={ROUTES.HOME}
                            src="/images/icons/logo.svg"
                            alt="Netflix"
                        />
                    </Header.Group>


                    <Header.Group>
                        <Link to={ROUTES.BROWSE}>Browse</Link>
                    </Header.Group>

                    <Header.Group>
                        <Link to={ROUTES.PROFILES}>Profiles</Link>
                    </Header.Group>


                    <Nav/>

                    <Header.Group>
                        <Header.Profile>
                            <Header.Picture src={user.photoURL}/>
                            <Header.Dropdown>
                                <Header.Group>
                                    <Header.Picture src={user.photoURL}/>
                                    <Header.Link>
                                        {user.displayName}
                                    </Header.Link>
                                </Header.Group>
                                <Header.Group>
                                    <Header.Link
                                        onClick={() =>
                                            firebase.auth().signOut()
                                        } to="/browse"
                                    >
                                        Sign out
                                    </Header.Link>
                                </Header.Group>
                            </Header.Dropdown>

                        </Header.Profile>
                    </Header.Group>
                </Header.Frame>

                <Header.Feature>
                    <Header.FeatureCallOut>
                        Unlimited movies, TV shows, and more.
                    </Header.FeatureCallOut>
                    <Header.Text>
                        Netflix is the home of amazing original programming that you can’t find anywhere else. Movies,
                        TV shows, specials and more, all tailored specifically to you.
                    </Header.Text>
                </Header.Feature>
            </Header>

            <div className="app-wrapper-content">
                <h1>Profiles</h1>
                <h3>User name: {user.displayName}</h3>
                <h4>E-mail: {user.email}</h4>{<br/>}
                {<br/>}

            </div>
        </div>
    );
}


// export default Profiles;


