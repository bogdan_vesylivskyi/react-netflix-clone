import { useState, useEffect, useContext } from "react"
import { FirebaseContext } from "../context/firebase"

export default function useAuthListener() {
    const [user, setUser] = useState(
        JSON.parse(localStorage.getItem("authUser"))
    )
    const { firebase } = useContext(FirebaseContext)

    // console.log(user, 'user, setUser')

    useEffect(() => {
        const listener = firebase.auth().onAuthStateChanged((authUser) => {
            if (authUser) {
                localStorage.setItem("authUser", JSON.stringify(authUser))
                setUser(authUser)
                // console.log(authUser, 'user')
            } else {
                localStorage.removeItem("authUser")
                setUser(user)
            }
        })
        return () => listener()
    }, [])

    return { user }
}
